# Example Code for CI/CD talk

# Disclaimer

Here we try to evaluate the CPU usage over time for docker build/run/ ... commands. The value of the CPU usage over time is a hint only for the cosumed energy of the process running the command. But it can be a good hint for making sustainable decission when working with docker.



# Usage

RUN

```bash
./topp docker build -t first ./first
```
to ge the CPU usage over time of the build for the first docker image. 

Give the other images a try also.