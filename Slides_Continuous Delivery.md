## Nachhaltigkeit in der Deploy Pipeline
### Continuous Lifecycle 2019

Note:

Abstract (Nicht sichtbar in Presentation Mode):

Natürlich können wir als Entwickler noch immer nicht auf genaue Zahlen aus der Wissenchaft berufen. Ein Tool, welches uns den Energieverbrauch eines Jobs in der CI sichbar macht scheint derzeit noch nicht möglich. Doch allein mit gesunden Menschnverstand lassen sich schon Regeln aufstellen, wie ich die Jobs zusammenführe um mir als Entwickler die Sicherheit meiner Tests zu gewährleisten, aber auch um am Ende nicht unzählige Docker Images umsonst gebaut zu haben. Darum lasst uns gemeinsam auf eine Reise vom Aufbau eines Docker-Images bishin zum fertigen Build antreten, um die CI - Lösungen in der Entwicklung bereits mit zu hohen C0_2-Werten konfrontiert zu sehen. Denn das ist ein Teil, den wir selbst in der Hand haben.

---

# Continuous Delivery

### Continuous Integration

Note:

* Gemeinsame Codebasis
* häufige Integration
* gespiegelte Testumgebung
* Einfacher Zugriff

---

# Continuous Deliveery

### Testing

Note:

* Wir haben natürlich auch eine Automatisierung von Tests
* dazu eben auch Reporting und das Deployment
* alls Automatisiert abgebildet in Jobs unseres CI-Tools der Wahl
* Automatisierungen (Testing, Reporting, Deployments)
* Testing | kurze Testzyklen

---

# Continuous Delivery

### Automatism

Note:

* nict nur die Tests laufen automatisch. der gesammt build wir hier völlig autak geführt.
* Im Grunde muss ich als Entwickler Nichts mehr tun.

---

# Continuous Delivery

### Agile Development Process

---

# Sustainability

Note:

* Was hat das nun mit Nachhaltigkeit zu tun?
* Natürlich führt dieser Weg zu nachhaltiger Software
* Nachhaltig im Sinne von:
* * Solide
* * Qualitativ gut in der Form, dass wir schnell Features einpflegen können
* Es führt im Grunde dazu, dass unsere Branche Beweglicher ist als viele andere traditionelle Unternehmen

---

# Sustainability - Resources

Note:

* Ich habe natürlich auch eine andere Defintion im Kopf
* mir geht es auch um die Schonung unserer Resourcen, die uns der Planet gibt.

---

# Resources

### Developers

Note:

* Es mag wieder blöde klingen, aber ich zähle uns Entwickler hier auch zu den Resourcen.
* Entwickler sind rarr und ich hoffe, dass ordentlich mit ihnen umgegangen wird, ansonsten rate ich nur jedem sich einen passenden Arbeitgeber zu suchen
* Doch was haben Entwickler hier von CI?
* Automatismen sorgen dafür, dass Entwickler nicht verheitzt werden.
* Automatismen entlasten den Entwickler bei seinem eigentlichen Job
* * Testing könnte ja auch per hand laufen
* * Testing kann uns nicht anlügen, wenn wir es nicht genauso vorher programmiert haben
* Zu den Automatismen gehören natürlich auch Artefakte der Entwicklung dazu wie bspw. das CodeReview, oder auch ein sauberes Grooming

---

# Resources

### Mashines

Note:

* "Die Welt ist nicht genug" so oder so ähnlich könnte wohl auch unser Leitspruch heißen.
* Wenn wir nicht gerade in einer kleinen Agentur arbeiten, wo 100 Wordpress-Instanzen auf einer Machine laufen (daher kommt wohl auch das press in WP), dann sind wir doch in der schönen Lage, dass Resourcen im Sinne von Hardware aktuell noch keine rolle spielt.
* unsere Mashinenen werden zu langsam, was solls wir tauschen sie aus.
* die Pipelines laufen zu langsam, was solls wir paralellisieren .. neue Runner und gut ist
* Ähnlich läuft es doch auch noch in den Applikationen, oder wie behebt ihr Performance-Probleme?

---

# Resources

### Energy

Note: 

* Während ich bei Mashinen die Rohstoffe für die Herstellung meine, so
* meine ich hier den Betrieb der Maschinen.
* und genau da haben wir als Branche unseren größten Footprint, wenn wir über CO2 Emmissionen sprechen

---

# Responsibility

Note:

* daraus ergibt sich unsere Verantwortung
* hat irgend jemand eine Ahnung in welcher Größenordnung die IKT Anteil an den Weltweiten Emissionen hat?

---

### 3% Global Greenhouse Gas Emissions

---

### 10% of global electric power consumption

---

### 2.5 kg CO_2 for every GB data

---

## What to do?

---

# First: Awareness

Note:

* das ist auch mit der Grund warum ich heute hier bin.
* Ich denke ich werde hier keine vollen Lösungen präsenieren können
* doch ich will euch zum nachdenken anregen

---

![](https://i.imgur.com/JWBXoRR.png "DevelopersForFuture" =60%x80%)

---

## Second: Avoid lazy Emissions

Note:

* Es mag schwer sein, aber wir müssen uns eingestehen, dass wir in der Art wie wir leben
* nicht weiter machen können.
* Wir müssen uns eingestehen, dass dass keine Resource unendlich ist. Somit sollten wir
* nach der Awareness eben auch Konsequnezen folgen lassen
* und wenn wir einmal in uns gehen, gibt es da einiges oder?

---

# Bare Metal
### vs.
# Docker Image

Note:

* aus dem Meta bin ich jetz raus, drum möchte ich nun auch endlich mal technisch werden
* Wenn wir über CI/CD reden möchte ich bitte nur noch über containerized Apps reden.
* Bare Metal ist in vielerlei hinsicht einfach Kacke und erst Recht wenn wir über Resourcen Schonen reden 

---

# Bare Metal

* Systemresourcen unausgelastet
* sperrig
* sharred Hosting?
* One fits it all - Solutions
* Zeit beim Bau

Note:

* das was da steht
* ich kann resourcen nicht so vergeben, wie es die app bräuchte

---

# Build Docker Images

Note:

* Dann gehen wir doch gleich zu Images 
* Und ich möchte hier gleich einmal auf zwei Pattern eingehen, die wir doch alle irgendwie gelernt haben

---

# First Example

```Dockerfile
FROM node:13

# install chrome for protractor tests
RUN \
  wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
  echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list && \
  apt-get update && \
  apt-get install -y google-chrome-stable && \
  rm -rf /var/lib/apt/lists/*

# install and cache app dependencies
COPY ./ .

# add `/app/src/node_modules/.bin` to $PATH
ENV PATH /app/src/node_modules/.bin:$PATH

WORKDIR /app/src

RUN npm install && npm install -g @angular/cli

# start app
CMD ng serve --host 0.0.0.0 --watch
```

---

# First Example

![](https://i.imgur.com/LNOZqKs.jpg "First Image" =60%x80%)


---

# Multistage Example


```Dockerfile
FROM node:13 as builder

COPY ./ .
ENV PATH /app/src/node_modules/.bin:$PATH
WORKDIR /app/src
RUN npm install && npm install -g @angular/cli
FROM node:13
RUN \
  wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
  echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list && \
  apt-get update && \
  apt-get install -y google-chrome-stable && \
  rm -rf /var/lib/apt/lists/*
COPY ./ .
COPY --from=builder /app/src/node_modules /app/src/
# add `/app/src/node_modules/.bin` to $PATH
ENV PATH /app/src/node_modules/.bin:$PATH
WORKDIR /app/src
RUN npm install -g @angular/cli
# start app
CMD ng serve --host 0.0.0.0 --watch
```

---

# Multistage Example

![](https://i.imgur.com/dQ0Y8NU.jpg "Multistage Image" =60%x80%)


---

# Second Example

```Dockerfile
FROM node:13

# install chrome for protractor tests
RUN \
  wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
  echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list && \
  apt-get update && \
  apt-get install -y google-chrome-stable && \
  rm -rf /var/lib/apt/lists/*

```

---

# Second Exampl

![](https://i.imgur.com/hBPxZ8Y.jpg "Second Base Image" =60%x80%)


---

# Second Example

```Dockerfile
FROM second/base

# install and cache app dependencies
COPY ./ .

# add `/app/src/node_modules/.bin` to $PATH
ENV PATH /app/src/node_modules/.bin:$PATH

WORKDIR /app/src

RUN npm install && npm install -g @angular/cli

# start app
CMD ng serve --host 0.0.0.0 --watch

```

---

# Second Example

![](https://i.imgur.com/MvZjEjh.jpg "Second app Image" =60%x80%)

---

# Third Example

```Dockerfile
FROM node:13

# install chrome for protractor tests
RUN \
  wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
  echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list && \
  apt-get update && \
  apt-get install -y google-chrome-stable && \
  rm -rf /var/lib/apt/lists/*

RUN npm install && npm install -g @angular/cli
```

---

# Third Example

![](https://i.imgur.com/f7iYhjn.jpg "Third Base Image" =60%x80%)

---

# Third Example

```Dockerfile
FROM third/base

# install and cache app dependencies
COPY ./ .

# add `/app/src/node_modules/.bin` to $PATH
ENV PATH /app/src/node_modules/.bin:$PATH

WORKDIR /app/src

# start app
CMD ng serve --host 0.0.0.0 --watch
```

---

# Third Example


![](https://i.imgur.com/Zvhbqx2.jpg "Third app Image" =60%x80%)

---

# Pipelines

Note:

* einige definieren und zeiten anschauen.


---

# Conclusion

* CI/CD ist nachhaltig für Software aber auch Resourcen
* Images Bau hat Einfluss auf den Footprint meiner Dev-Arbeit
* Pipelines ausgewogen und durchdacht

----

# Dear Scientists/Enginiers

# We need Numbers!

---

# Links

* [DevelopersForFuture](https://developersforfuture.org)
---

# Thanks














